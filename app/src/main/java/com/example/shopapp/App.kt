package com.example.shopapp

import android.app.Application
import android.content.Context
import com.example.shopapp.push_notifications.ExampleNotificationOpenedHandler
import com.onesignal.OneSignal

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        instance = this
        context = applicationContext
        OneSignal.startInit(this)
            .setNotificationOpenedHandler(ExampleNotificationOpenedHandler())
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }

    companion object {
        lateinit var instance: App
        private lateinit var context: Context
    }

    fun getContext() = context
}