package com.example.shopapp.network

import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @GET("{path}")
    fun getRequest(
        @Path("path") path: String?,
        @QueryMap parameters: MutableMap<String, String>
    ): Call<String>?



    @FormUrlEncoded
    @POST("{path}")
    fun postRequest(
        @Path("path") path: String?,
        @FieldMap parameters: MutableMap<String, String>
    ): Call<String>?
}