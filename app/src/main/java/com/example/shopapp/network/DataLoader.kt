package com.example.shopapp.network

import android.util.Log
import android.view.View
import com.example.shopapp.App
import com.example.shopapp.R
import com.example.shopapp.shared_preferences.UserPreferences
import okhttp3.OkHttpClient
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.HTTP
import java.util.concurrent.TimeUnit

object DataLoader {
//    chemi
    private const val HTTP_200_OK = 200
    private const val HTTP_201_CREATED = 201
    private const val HTTP_400_BAD_REQUEST = 400
    private const val HTTP_401_UNAUTHORIZED = 401
    private const val HTTP_404_NOT_FOUND = 404
    private const val HTTP_500_INTERNAL_SERVER_ERROR = 500
    private const val HTTP_204_NO_CONTENT = 204

    private val httpClient = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.MINUTES)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .addInterceptor { chain ->
            val token = UserPreferences.getString(UserPreferences.TOKEN)!!
            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
            if (token.isNotEmpty())
                request.addHeader("Authorization", "bearer $token")
            chain.proceed(request.build())
        }


    private val retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(App.instance.getContext().getString(R.string.domain))
        .client(httpClient.build())
        .build()


    var service: ApiService = retrofit.create(ApiService::class.java)

    fun getRequest(
        loadingView: View? = null,
        path: String,
        parameters: MutableMap<String, String>,
        shopCallBack: ShopCallBack
    ) {
        val repos: Call<String> = service.getRequest(path, parameters)!!
        repos.enqueue(handleResponse(loadingView, shopCallBack))
    }

    fun postRequest(
        loadingView: View? = null,
        path: String,
        parameters: MutableMap<String, String>,
        shopCallBack: ShopCallBack
    ) {
        if (loadingView != null)
            loadingView.visibility = View.VISIBLE
        val repos: Call<String> = service.postRequest(path, parameters)!!
        repos.enqueue(handleResponse(loadingView, shopCallBack))
    }

    private fun handleResponse(loadingView: View? = null, shopCallBack: ShopCallBack): Callback<String> {
        return object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {

            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (loadingView != null)
                    loadingView.visibility = View.GONE
                if (response.code() == HTTP_200_OK || response.code() == HTTP_201_CREATED)
                    try {
                        shopCallBack.onResponse(response.body()!!)
                    } catch (e: JSONException) {
                        shopCallBack.onFailure(
                            App.instance.getContext().getString(R.string.incorrect_request),
                            App.instance.getContext().getString(R.string.error_occurred)
                        )
                    }
                else if (response.code() == HTTP_400_BAD_REQUEST) {
                    handleError(response.errorBody()!!.string(), shopCallBack)
                } else if (response.code() == HTTP_401_UNAUTHORIZED) {
                    handleError(response.errorBody()!!.string(), shopCallBack)
                } else if (response.code() == HTTP_404_NOT_FOUND) {
                    handleError(response.errorBody()!!.string(), shopCallBack)
                } else if (response.code() == HTTP_500_INTERNAL_SERVER_ERROR) {
                    handleError(response.errorBody()!!.string(), shopCallBack)
                } else if (response.code() == HTTP_204_NO_CONTENT) {
                    handleError(response.errorBody()!!.string(), shopCallBack)
                } else {
                    shopCallBack.onFailure(
                        App.instance.getContext().getString(R.string.incorrect_request),
                        App.instance.getContext().getString(R.string.error_occurred)
                    )
                }
            }
        }

    }

    private fun handleError(errorBody: String, shopCallBack: ShopCallBack) {
        val errorJson = JSONObject(errorBody)
        Log.d("errorJson", " $errorJson")
        if (errorJson.has("error")) {
            shopCallBack.onFailure(
              App.instance.getContext().resources.getString(R.string.incorrect_request),
                errorJson.getString("error")
            )
        }
    }

}