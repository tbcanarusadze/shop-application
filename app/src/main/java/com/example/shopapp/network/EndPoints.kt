package com.example.shopapp.network

object EndPoints {
    const val REGISTER = "register"
    const val LOG_IN = "login"
    const val CREATE_POST = "create-post"
    const val POSTS = "posts"
    const val COMPLETE_PROFILE = "complete-profile"
    const val PROFILE = "profile"
    const val DELETE_POST = "delete-post"
}