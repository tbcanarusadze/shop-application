package com.example.shopapp.network

interface ShopCallBack {
    fun onFailure(title: String, error: String)
    fun onResponse(response: String)
}