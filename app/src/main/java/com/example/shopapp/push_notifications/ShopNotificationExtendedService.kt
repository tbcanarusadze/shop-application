package com.example.shopapp.push_notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ClipDescription
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.shopapp.R
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import kotlin.properties.Delegates

class ShopNotificationExtendedService : NotificationExtenderService() {

    private var actionId by Delegates.notNull<Int>()
    private lateinit var title: String
    private lateinit var description: String
    private lateinit var postId: String

    override fun onNotificationProcessing(notification: OSNotificationReceivedResult): Boolean {
        val json = notification.payload.additionalData
        actionId = json.getInt("action_id")
        title = json.getString("title")
        description = json.getString("description")
        postId = json.getString("postId")

        createNotificationChannel()
        return false

    }

    private val intent = Intent(applicationContext, PostDetailActivity::class.java).apply {
        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    }
    private val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

    private val builder = NotificationCompat.Builder(this, actionId.toString())
        .setSmallIcon(R.mipmap.ic_launcher)
        .setContentTitle(title)
        .setContentText(description)
        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        .setContentIntent(pendingIntent)
        .setAutoCancel(true)

    private fun createNotificationChannel() {
        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.title_text)
            val descriptionText = getString(R.string.description_text)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(actionId.toString(), name, importance).apply {
                description = descriptionText
            }

            notificationManager.createNotificationChannel(channel)
            notificationManager.notify(1, builder.build())
        }
    }


}