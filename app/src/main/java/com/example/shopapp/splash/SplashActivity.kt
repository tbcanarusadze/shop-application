package com.example.shopapp.splash

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.shopapp.R
import com.example.shopapp.shared_preferences.UserPreferences
import com.example.shopapp.ui.authorization.login.LogInActivity
import com.example.shopapp.ui.MainActivity

class SplashActivity : AppCompatActivity() {

    private val handler = Handler()
    private val runnable = Runnable {
        openActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onStart() {
        super.onStart()
        postDelayed()
    }

    override fun onPause() {
        super.onPause()
        removeCallBack()
    }


    private fun openActivity() {
        val activity: Activity =
            if (UserPreferences.getString(UserPreferences.SESSION)!!.isEmpty()){
                UserPreferences.clearAll()
                LogInActivity()
            }
            else
                MainActivity()

        val intent = Intent(this, activity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    private fun postDelayed() {
        handler.postDelayed(runnable, 3000)
    }

    private fun removeCallBack() {
        handler.removeCallbacks(runnable)
    }

}
