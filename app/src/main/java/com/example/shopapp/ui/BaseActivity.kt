package com.example.shopapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.shopapp.R

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }


    protected fun onBack() {
        super.onBackPressed()
        overridePendingTransition(
            android.R.anim.fade_in, android.R.anim.fade_out
        )
    }

    override fun onBackPressed() {
        onBack()
    }
}
