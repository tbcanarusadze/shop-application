package com.example.shopapp.ui.authorization.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.example.shopapp.R
import com.example.shopapp.extensions.isEmailValid
import com.example.shopapp.extensions.setColor
import com.example.shopapp.network.DataLoader
import com.example.shopapp.network.EndPoints
import com.example.shopapp.network.ShopCallBack
import com.example.shopapp.shared_preferences.UserPreferences
import com.example.shopapp.tools.Tools
import com.example.shopapp.ui.authorization.profile.CompleteProfileActivity
import com.example.shopapp.ui.authorization.profile.ProfileModel
import com.example.shopapp.ui.authorization.signup.SignUpActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.loader_layout.*

class LogInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        init()
    }

    private fun init() {
        setUpSignUpTextView()
        logEmail.isEmailValid(this)
    }

    private fun setUpSignUpTextView() {
        signUpTextView.setColor(
            " ${getString(R.string.new_user)}",
            ContextCompat.getColor(this, R.color.textColor)
        )
        signUpTextView.setColor(
            " ${getString(R.string.sign_up)}",
            ContextCompat.getColor(this, R.color.colorPrimary)
        )
        signUpTextView.setColor(
            " ${getString(R.string.here)}",
            ContextCompat.getColor(this, R.color.textColor)
        )
    }

    fun rememberMe(view: View) {
        if (rememberMeImage.tag == "0") {
            rememberMeImage.setImageResource(R.mipmap.ic_remember_me)
            rememberMeImage.tag = "1"
        } else {
            rememberMeImage.setImageResource(R.mipmap.ic_uncheck_remember_me)
            rememberMeImage.tag = "0"
        }
    }

    fun signUp(view: View) {
        startActivity(Intent(this, SignUpActivity::class.java))
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    fun signIn(view: View) {
        val email = logEmail.text.toString()
        val password = logPassword.text.toString()
        when {
            logEmail.tag == "0" -> Tools.initDialog(
                this,
                getString(R.string.incorrect_request),
                getString(R.string.email_not_valid)
            )
            password.isNotEmpty() -> {
                spinKitContainerView.visibility = View.VISIBLE
                val parameters = mutableMapOf<String, String>()
                parameters["email"] = email
                parameters["Password"] = password
                DataLoader.postRequest(
                    spinKitContainerView,
                    EndPoints.LOG_IN,
                    parameters,
                    object : ShopCallBack {
                        override fun onResponse(response: String) {
                            val logInModel = Gson().fromJson(response, LogInModel::class.java)
                            if (rememberMeImage.tag == "1")
                                UserPreferences.saveString(
                                    UserPreferences.SESSION,
                                    logInModel.userId!!
                                )
                            UserPreferences.saveString(UserPreferences.USER_ID, logInModel.userId!!)
                            UserPreferences.saveString(UserPreferences.TOKEN, logInModel.token!!)
                            getProfile(logInModel.userId)
                        }

                        override fun onFailure(title: String, error: String) {
                            Tools.initDialog(
                                this@LogInActivity,
                                title,
                                error
                            )
                        }
                    })
            }
            else -> Tools.initDialog(
                this,
                getString(R.string.incorrect_request),
                getString(R.string.fill_all_fields)
            )
        }
    }

    private fun getProfile(userId: String) {
        val parameters = mutableMapOf<String, String>()
        parameters["user_id"] = userId
        DataLoader.postRequest(
            spinKitContainerView,
            EndPoints.PROFILE,
            parameters,
            object: ShopCallBack{
                override fun onFailure(title: String, error: String) {

                }

                override fun onResponse(response: String) {
                    val profile = Gson().fromJson(response, ProfileModel::class.java)
                    if(!profile.profileCompleted){
                        startActivity(Intent(this@LogInActivity, CompleteProfileActivity::class.java))
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                    }
                }
            })
    }


}
