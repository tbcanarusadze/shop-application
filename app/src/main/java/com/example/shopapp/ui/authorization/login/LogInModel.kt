package com.example.shopapp.ui.authorization.login

import com.google.gson.annotations.SerializedName

class LogInModel {
    @SerializedName("user_id")
    val userId: String? = null
    val token: String? = null
}