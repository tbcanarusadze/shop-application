package com.example.shopapp.ui.authorization.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProfileModel(
    @SerializedName("profile_completed")
    val profileCompleted: Boolean = false,
    val email: String,
    @SerializedName("full_name")
    val fullName: String,
    @SerializedName("profile_url")
    val profileUrl: String,
    val address: String,
    @SerializedName("card_number")
    val cardNumber: String,
    @SerializedName("card_holder_name")
    val cardHolderName: String,
    @SerializedName("expiry_date")
    val expiryDate: String,
    @SerializedName("security_code")
    val securityCode: String
) :
    Parcelable