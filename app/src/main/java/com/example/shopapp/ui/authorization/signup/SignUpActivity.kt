package com.example.shopapp.ui.authorization.signup
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.example.shopapp.R
import com.example.shopapp.extensions.isEmailValid
import com.example.shopapp.extensions.setColor
import com.example.shopapp.network.DataLoader
import com.example.shopapp.network.EndPoints
import com.example.shopapp.network.ShopCallBack
import com.example.shopapp.tools.Tools
import com.example.shopapp.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.loader_layout.*
import org.json.JSONObject

class SignUpActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        setUpSignInTextView()
        regEmail.isEmailValid(this)
    }

    private fun setUpSignInTextView() {
        signInTextView.setColor(
            " ${getString(R.string.already_member)}",
            ContextCompat.getColor(this, R.color.textColor)
        )
        signInTextView.setColor(
            " ${getString(R.string.sign_in)}",
            ContextCompat.getColor(this, R.color.colorPrimary)
        )

    }

    fun signUp(view: View) {
        val email = regEmail.text.toString()
        val fullName = regFullName.text.toString()
        val password = regPassword.text.toString()
        val password2 = regRepeatPassword.text.toString()

        if (email.isEmpty() || fullName.isEmpty() || password.isEmpty() || password2.isEmpty()) {
            Tools.initDialog(
                this,
                getString(R.string.incorrect_request),
                getString(R.string.fill_all_fields)
            )
        } else if (regEmail.tag == "0")
            Tools.initDialog(
                this,
                getString(R.string.incorrect_request),
                getString(R.string.email_not_valid)
            )
        else if (password != password2)
            Tools.initDialog(
                this,
                getString(R.string.incorrect_request),
                getString(R.string.passwords_not_match)
            )
        else {
            val parameters = mutableMapOf<String, String>()
            parameters["email"] = email
            parameters["full_name"] = fullName
            parameters["password"] = password
            DataLoader.postRequest(
                spinKitContainerView,
                EndPoints.REGISTER,
                parameters,
                object : ShopCallBack {
                    override fun onFailure(title: String, error: String) {
                        Tools.initDialog(
                            this@SignUpActivity,
                            title,
                            error
                        )
                    }

                    override fun onResponse(response: String) {
                        val json = JSONObject(response)
                        if (json.has("registered") && json.getBoolean("registered"))
                            onBack()
                    }
                })
        }
    }


    fun signIn(view: View) {
        onBack()
    }


}
